extends Popup


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pause_mode = PAUSE_MODE_PROCESS
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
func _input(_event):
	if Input.is_action_just_released("ui_accept"):
		get_tree().paused = true
		popup()
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()



func _on_exit_pressed():
	get_tree().paused = false
	hide()
	pass # Replace with function body.


func _on_restart_pressed():
	get_tree().paused = false
	get_tree().reload_current_scene()
	#get_node("../Player/ThreeOrb").replace_by("res://scenes/3orb.tscn")
	pass # Replace with function body.


func _on_options_pressed():
	pass # Replace with function body.
