extends Node2D

export var speed = 6
var health = 6
var Boom = false


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	
	if health > 6:
		health = 6
	OrbAnim()
	move(delta)
		
func move(delta):	
		set_rotation( global_position.angle_to_point(get_global_mouse_position()))
		position = position.linear_interpolate(get_global_mouse_position(),delta*speed)
	

func OrbAnim():
	match health:
		
		6:
			$AnimatedSprite.set_animation("6 health") 
		5:
			$AnimatedSprite.set_animation("5 health") 
		4:
			$AnimatedSprite.set_animation("4 health") 
		3:
			$AnimatedSprite.set_animation("3 health") 
		2:
			$AnimatedSprite.set_animation("2 health") 
		1:
			$AnimatedSprite.set_animation("1 health") 
		0: 
			$AnimatedSprite.set_animation("death") 



func _on_Area2D_area_entered(area):
	if area.filename == "res://scenes/Bullet.tscn":
		if area.color == 1:
			health +=1
		else:
			health -= 1
			$Hit.play()
			$AnimatedSprite.set_animation("damage")
			
		if area.color == 1 and health ==6:
			$Powerup.play()
			$AnimatedSprite.set_animation("powerup")	
		if health <= 0:
			$Death.play()
		area.queue_free()
	else:
		pass # Replace with function body.



func _on_Boomer_area_entered(area):
	if Boom == true and area.filename == "res://scenes/Bullet.tscn":
		#$Boomer/CollisionShape2D.shape.radius = 10/ ($Timer.get_time_left() + 0.1)#136
		print($Boomer/CollisionShape2D.shape.radius )
		$Boom.show()
		$Boom.play()
		area.queue_free()


func _on_Timer_timeout():
	Boom = false
	$Boom.hide()
