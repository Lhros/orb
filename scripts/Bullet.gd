extends Area2D

var color
var speed = 6

var ymove = 0.0
var yMulti = 1.0

var fast = false
var big = false 
var rotate = false

# Called when the node enters the scene tree for the first time.
func _ready():
	
	if fast == true:
		speed = 10
	if big ==true:
		set_scale(Vector2(2.0,2.0))
	if rotate == true and $Timer.is_stopped() == true:
		$Timer.start()
		ymove = 4.0


	match color:
		1:
			$AnimatedSprite.play("blue")		
		2:
			$AnimatedSprite.play("yellow")			
		3:	
			$AnimatedSprite.play("purple")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(_delta):
		position += Vector2(-2*speed,ymove*yMulti)
	

func _on_Timer_timeout():
	yMulti = yMulti*-1
