extends Node2D

export var speed = 6

var health = 6
onready var nodeTarget = get_parent().get_node("TwoOrb/target").global_position

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	
	if health > 6:
		health = 6
	OrbAnim()
	Move(delta)
		
func Move(delta):	
	if get_parent().get_parent().twoOrbHealth <= 0 and get_parent().get_parent().oneOrbHealth > 0 :
		nodeTarget = get_parent().get_node("OneOrb/target").global_position
	elif get_parent().get_parent().twoOrbHealth > 0:
		nodeTarget = get_parent().get_node("TwoOrb/target").global_position
	elif get_parent().get_parent().oneOrbHealth <= 0 and get_parent().get_parent().twoOrbHealth <=0 :
		nodeTarget = get_global_mouse_position()
		
	if global_position.distance_to(nodeTarget) > 80:
		
		set_rotation( global_position.angle_to_point(nodeTarget))
		position = position.linear_interpolate(nodeTarget,delta*speed)
		
	if get_parent().get_parent().oneOrbHealth <= 0 and get_parent().get_parent().twoOrbHealth <= 0:
		set_rotation( global_position.angle_to_point(get_global_mouse_position()))
		position = position.linear_interpolate(get_global_mouse_position(),delta*speed)


func OrbAnim():
	match health:
		
		6:
			$AnimatedSprite.set_animation("6 health") 
		5:
			$AnimatedSprite.set_animation("5 health") 
		4:
			$AnimatedSprite.set_animation("4 health") 
		3:
			$AnimatedSprite.set_animation("3 health") 
		2:
			$AnimatedSprite.set_animation("2 health") 
		1:
			$AnimatedSprite.set_animation("1 health") 
		0: 
			$AnimatedSprite.set_animation("death") 


func _on_Area2D_area_entered(area):
	if area.filename == "res://scenes/Bullet.tscn":
		if area.color == 3:
			health +=1
		else:
			health -= 1
			$Hit.play()
			$AnimatedSprite.set_animation("damage")
		
		if area.color == 1 and health ==6:
			$Powerup.play()
			$AnimatedSprite.set_animation("powerup")	
		if health <= 0:
			$Death.play()
		area.queue_free()
	else:
		pass
