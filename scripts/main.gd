extends Node2D



var oneOrbHealth = 6
var twoOrbHealth = 6
var threeOrbHealth = 6

var bullet = preload("res://scenes/Bullet.tscn")

var score = 0.0



# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _process(_delta):
	healthcheck()
	fscore()
	PowerUpAni()
	GameOver()
	Diff()
	print($Timer.get_wait_time())
	
func Diff():
	$Timer.set_wait_time(0.2- (score*0.00001))
#	if	score >1500:
#			$Timer.set_wait_time(0.15)
#	if	score>2500:
#			$Timer.set_wait_time(0.10)
#	if	score>4500:
#			$Timer.set_wait_time(0.05)

func fscore():
		score += get_physics_process_delta_time()*100
		$scorebox.set_text(str(score))
func Boom():
	if oneOrbHealth == 6:
		get_node("Player/OneOrb").Boom = true
		get_node("Player/OneOrb/Timer").start(1.0)
		get_node("Player/OneOrb").health -= 3
func Slow():
	if twoOrbHealth == 6:
		get_node("Player/TwoOrb").Slow = true	
		get_node("Player/TwoOrb/Timer").start(3.0)
		get_node("Player/TwoOrb").health -= 3

func Heal():
	if threeOrbHealth == 6:
		if get_node("Player/OneOrb") != null:
			get_node("Player/OneOrb").health += 2
		
		if get_node("Player/TwoOrb") != null:
			get_node("Player/TwoOrb").health += 2
		
		if get_node("Player/ThreeOrb") != null:
			get_node("Player/ThreeOrb").health -= 3

func GameOver():
	if oneOrbHealth == 0 and twoOrbHealth ==0 and threeOrbHealth ==0:
		get_tree().paused = true
		$Pause.popup()
		$Pause/Pause.hide()
		$Pause/GameOver.show()
		
func PowerUpAni():
	if oneOrbHealth == 6:
		$Control/Boom.play("Ready")
	else:
		$Control/Boom.play("Spent")
		
	if twoOrbHealth == 6:
		$Control/Slow.play("Ready")
	else:
		$Control/Slow.play("Spent")
		
	if threeOrbHealth == 6:
		$Control/Heal.play("Ready")
	else:
		$Control/Heal.play("Spent")
		

	
func healthcheck():
	if oneOrbHealth > 0:
		oneOrbHealth = get_node("Player/OneOrb").health
	elif get_node("Player").has_node("OneOrb") == true:
		get_node("Player/OneOrb").queue_free()
	
	if twoOrbHealth > 0:
		twoOrbHealth = get_node("Player/TwoOrb").health
	elif get_node("Player").has_node("TwoOrb") == true:
		get_node("Player/TwoOrb").queue_free()
	
	if threeOrbHealth > 0:
		threeOrbHealth = get_node("Player/ThreeOrb").health
	elif get_node("Player").has_node("ThreeOrb") == true:
		get_node("Player/ThreeOrb").queue_free()


func _input(_event):

	if Input.is_action_just_pressed('d'):

		Boom()
		pass
	if Input.is_action_just_pressed('s'):

		Slow()
		pass
	if Input.is_action_just_pressed('a'):

		Heal()
		pass

	
func bullet_setter():
	var bullet_instance = bullet.instance()
	bullet_instance.position = Vector2(1600,rand_range(-100,750))
	bullet_instance.color = randi() % 3 + 1
	
	if oneOrbHealth <= 0:
		bullet_instance.fast = true
	if twoOrbHealth <= 0:
		bullet_instance.big = true
	if threeOrbHealth <= 0:
		bullet_instance.rotate = true
		
	get_parent().call_deferred("add_child",bullet_instance)
	


func _on_BottomWarpGutter2_area_entered(area):
	pass


func _on_TopWarpGutter_area_entered(area):
	pass
	


func _on_BulletGarbage_area_entered(area):
	
	if area.name != "BottomWarpGutter" or "TopWarpGutter":
		area.queue_free()
		#bullet_setter()


func _on_Timer_timeout():
	bullet_setter()


